# selenium modules
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions


class GoogleMainPage():
    # contructor method, where the driver, search and input xpath's are located
    def __init__(self, driver):
        self.driver = driver
        self.search = None
        self.xpathSearchInput = "//input[@name='q']"

    def askForSearch(self):
        self.search = input('Type what you want to search: ')

    def writeSearch(self):
        wait = WebDriverWait(self.driver, 30)
        searchInput = wait.until(expected_conditions.element_to_be_clickable(
            (By.XPATH, self.xpathSearchInput)))
        searchInput.send_keys(self.search)

    def pressEnter(self):
        wait = WebDriverWait(self.driver, 30)
        searchInput = wait.until(expected_conditions.element_to_be_clickable(
            (By.XPATH, self.xpathSearchInput)))
        searchInput.send_keys(Keys.ENTER)
