# selenium modules
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

# miscellaneous modules
from miscellaneous import colours as cl


class GoogleResultsPage:
    # contructor method, where the driver, search and input xpath's are located
    def __init__(self, driver):
        self.driver = driver
        self.search = None
        self.xpathSearchInput = "//div[@class='a4bIc']//input[@name='q']"
        self.xpathResults = "//h3[@class='LC20lb DKV0Md']"
        self.resultsList = None
        self.filteredResults = []

    """this method takes the value of the search input to be able
    to compare it with the found results"""

    def setSearch(self):
        wait = WebDriverWait(self.driver, 30)
        searchInput = wait.until(expected_conditions.element_to_be_clickable(
            (By.XPATH, self.xpathSearchInput)))

        self.search = searchInput.get_attribute('value').upper()

    """this method gets the results of the results page and
    storages them in a list"""

    def getResults(self):
        wait = WebDriverWait(self.driver, 60)
        self.resultsList = wait.until(
            expected_conditions.presence_of_all_elements_located((By.XPATH, self.xpathResults)))

    """this method verifies if the results contain the
    searched word(s)"""

    def compareResults(self):
        for res in self.resultsList:
            if self.search in res.get_attribute('innerText').upper():
                self.filteredResults.append(
                    res.get_attribute('innerText').upper())

    """this method prints the filtered results"""

    def printFilteredResults(self):
        print(f'\n{cl.Colours.WHITE}:: FOUND [{len(self.filteredResults)}] RESULTS WITH COINCIDENCE\n')
        for i, res in enumerate(self.filteredResults):
            print(f'{cl.Colours.WHITE}{i + 1}. {res}')
