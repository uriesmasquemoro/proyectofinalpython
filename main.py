# built-in libraries
import sys

# selenium modules
from selenium import webdriver

# poms & miscellaneous modules
from miscellaneous import setup as st
from miscellaneous import colours as cl
from poms import googleMainPage as gmp
from poms import googleResultsPage as grp


def main():
    # setup & driver config
    setup = st.Setup()
    driver = setup.get_driver()
    driver.get('https://google.com/')

    # google main page instance
    googleMainPage = gmp.GoogleMainPage(driver)
    googleResultsPage = grp.GoogleResultsPage(driver)

    try:
        print(f'{cl.Colours.HEADER}:: GOOGLE SEACH ENGINE FROM TERMINAL')

        print('\n:: MENU')
        print('[1] Search with Google')
        print('[2] Exit')

        try:
            option = input('\nType a valid option: ')

            if option == '1':
                googleMainPage.askForSearch()
                googleMainPage.writeSearch()
                googleMainPage.pressEnter()
                googleResultsPage.setSearch()
                googleResultsPage.getResults()
                googleResultsPage.compareResults()
                googleResultsPage.printFilteredResults()
            elif option == '2':
                return
            else:
                print(f'{cl.Colours.WARNING}\n:: SELECT A VALID OPTION!')

        except Exception:
            print(f'{cl.Colours.FAIL}:: {sys.exc_info()[0]}')

        print(f'{cl.Colours.OKBLUE}\n:: GXIS LA REVIDO!\n')
        print(f'{cl.Colours.OKGREEN}:: MEMORY FREED')
        print(f'{cl.Colours.OKGREEN}:: EXIT SUCCESS')
        driver.quit()
    except:
        print(f'{cl.Colours.FAIL}:: {sys.exc_info()[0]}')
        print(f'{cl.Colours.OKGREEN}:: MEMORY FREED')
        driver.quit()


if __name__ == '__main__':
    main()
