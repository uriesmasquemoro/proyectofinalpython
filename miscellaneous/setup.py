from . import colours as cl
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

class Setup:
    def __init__(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='drivers/chromedriver80')
        print(f'{cl.Colours.OKGREEN}Driver set as Chrome')

    def get_driver(self):
        return self.driver